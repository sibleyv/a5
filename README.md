# LIS4368 - Advanced Web Applications Development

## Vincent Sibley

### Assignment 4 Requirements:

>1. Provide Bitbucket read-only access to a4 repo, *must* include README.md, using Markdown
syntax.
>
>2. Blackboard Links: a4 Bitbucket repo




#### Assignment Screenshots:

*Submit*:
![Submit](img/submit.png)
>
*Passed validation*:
![Passed validation](img/thanks.png)
>
*SQL*:
![SQL](img/sql.png)




